import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.css';
import {Game} from './Range/Game'

// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));
import React from 'react';
import {Row} from './Row'

export class Board extends React.Component {

    renderRow(i) {
        return (
            <Row
                squares={this.props.squares}
                row={i}
                key={i}
                onClick={j => this.props.onClick(j)}
            />
        );
    }

    renderLines() {
        let board = [];
        for (let i=0; i < 13; i++) {
            board.push(this.renderRow(i));
        }

        return board;
    }

    render() {

        return (
            <div>
                {this.renderLines()}
            </div>
        )
    }
}
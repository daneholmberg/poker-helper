import React from 'react';
import {Board} from './Board'
import prange from 'prange'

export class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            board: fillBoard()
        }
    }

    handleClick(i) {
        console.log(i);
    }

    render(){
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={this.state.board}
                        onClick={this.handleClick}
                    />
                </div>
            </div>
        );
    }
}

function fillBoard() {
    let duecesPlus = prange("22+");
    let A2o = prange("A2o+");
    let A2s = prange("A2s+");
    let K2o = prange("K2o+");
    let K2s = prange("K2s+");
    let Q2o = prange("Q2o+");
    let Q2s = prange("Q2s+");
    let J2o = prange("J2o+");
    let J2s = prange("J2s+");
    let T2o = prange("T2o+");
    let T2s = prange("T2s+");
    let _92o = prange("92o+");
    let _92s = prange("92s+");
    let _82o = prange("82o+");
    let _82s = prange("82s+");
    let _72o = prange("72o+");
    let _72s = prange("72s+");
    let _62o = prange("62o+");
    let _62s = prange("62s+");
    let _52o = prange("52o+");
    let _52s = prange("52s+");
    let _42o = prange("42o+");
    let _42s = prange("42s+");
    let _32o = prange("32o+");
    let _32s = prange("32s+");

    let board = Array(169).fill(null);
    for (let i=0; i < 13; i++) {
        board[(i * 13) + i] = duecesPlus[i];
        if (i < 12) {
            board[i + 1] = A2s[i];
            board[(i + 1) * 13] = A2o[i];
        }
        if (i < 11) {
            board[i + 15] = K2s[i];
            board[((i + 2) * 13) + 1] = K2o[i];
        }
        if (i < 10) {
            board[i + 29] = Q2s[i];
            board[((i + 3) * 13) + 2] = Q2o[i];
        }
        if (i < 9) {
            board[i + 43] = J2s[i];
            board[((i + 4) * 13) + 3] = J2o[i];
        }
        if (i < 8) {
            board[i + 57] = T2s[i];
            board[((i + 5) * 13) + 4] = T2o[i];
        }
        if (i < 7) {
            board[i + 71] = _92s[i];
            board[((i + 6) * 13) + 5] = _92o[i];
        }
        if (i < 6) {
            board[i + 85] = _82s[i];
            board[((i + 7) * 13) + 6] = _82o[i];
        }
        if (i < 5) {
            board[i + 99] = _72s[i];
            board[((i + 8) * 13) + 7] = _72o[i];
        }
        if (i < 4) {
            board[i + 113] = _62s[i];
            board[((i + 9) * 13) + 8] = _62o[i];
        }
        if (i < 3) {
            board[i + 127] = _52s[i];
            board[((i + 10) * 13) + 9] = _52o[i];
        }
        if (i < 2) {
            board[i + 141] = _42s[i];
            board[((i + 11) * 13) + 10] = _42o[i];
        }
        if (i < 1) {
            board[i + 155] = _32s[i];
            board[((i + 12) * 13) + 11] = _32o[i];
        }

    }

    return board
}
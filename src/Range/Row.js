import React from 'react';

export class Square extends React.Component {
    render() {
        let subclass = "";
        switch (this.props.value[this.props.value.length - 1]) {
            case ("s"):
                subclass = "square suited";
                break;
            case ("o"):
                subclass = "square offsuit";
                break;
            default:
                subclass = "square paired";
                break;
        }
        return (
            <button className={subclass} value={this.props.value} onClick={this.props.onClick}>
                {this.props.value}
            </button>
        );
    }
}

export class Row extends React.Component {

    renderSquare(i) {
        return (
            <Square
                value={this.props.squares[i]}
                key={this.props.squares[i]}
                onClick={() => this.props.onClick(this.props.squares[i])}
            />
        );
    }

    renderRow(j) {
        let board = [];
        for (let i=1; i < 14; i++) {
            board.push(this.renderSquare((i+(j * 13))-1));
        }

        return board;
    }

    render() {
        return (
            <div className="board-row">
                {this.renderRow(this.props.row)}
            </div>
        )
    }
}



